<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/Login.php");
class Register extends Login
{
    public function __construct() {
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
        
    }
    public function saveuser(){  
        try{
            $data = array();
            $email=strtolower(trim($this->input->post('email')));
            $firstName=ucfirst($this->input->post('firstname'));
            $lastName=ucfirst($this->input->post('lastname'));
            $password = $this->input->post('password'); 
            $referalCode = $this->input->post('referalcode'); 
            
            if($email=='' || $firstName=='' || $lastName=='' || $password=='')   
                return response_parameter_missing_message();

            $userInfo = $this->DataAccessLayer->getAll('UserMaster',array('Email'=>$email));
            
            if(isset($userInfo) && !empty($userInfo))
                return response_json_output(FALSE,$this->lang->line('user_exist'),$data); 

            if(!empty($referalCode)){
                $result= $this->check_referal_code($referalCode);
                if($result=='')
                    return response_json_output(false,'Referal Code is not valid',$data); 
            }                               
            $userMasterModel = array(
                    'FirstName'=>$firstName,
                    'LastName'=>$lastName,
                    'Email'=>$email,
                    'Password'=>md5($password),
                    'ReferalCode'=>generate_randomNumber()
                );
               
            $this->db->trans_begin();
            $userID = $this->DataAccessLayer->insert('UserMaster',$userMasterModel);
            $this->DataAccessLayer->logData('UserMaster',$userID,$userID);

            if(isset($result) && !empty($result))
                $this->save_invited_user($userID,$result[0]->ID);

            if ($this->db->trans_status() === FALSE)  $this->db->trans_rollback();               
            else $this->db->trans_commit();
            
            $this->send_mail($firstName,$lastName,$userID,$email);  

            $userInfo=$this->get_user_data($userID); 
          return response_success_message($userInfo);   
        }
        catch(Exception $exception){
           response_exception_message($exception);
        } 
    } 
    private function save_invited_user($userID,$invitedUserID){
       $referalID= $this->DataAccessLayer->insert('UserInvitedLink', array('UserID'=>$invitedUserID,'InvitedUserID'=>$userID,'earn'=>REFERAL_AMOUNT));
        $this->DataAccessLayer->logData('UserInvitedLink',$userID,$referalID);
    } 
    private function check_referal_code($referalCode){
        $result=$this->DataAccessLayer->getAll('UserMaster',array('ReferalCode'=>$referalCode));
        if(isset($result) && !empty($result))
            return $result;
        return '';
    }
    public function activation($id){
        $data = array();
        if(empty($id)) return;
        $userID =  base64_decode(urldecode($id));
        $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('IsEmailVerified'=>'Y'));
        $emaildata['name'] = $this->lang->line('congratulations');
        $emaildata['message'] = $this->lang->line('email_verified');      
        $content = $this->load->view('success_email',$emaildata, TRUE);
        echo $content;
    }
    private function send_mail($firstName,$lastName,$userID,$email){   
        $emaildata=array();
        $activationlink = base_url('register/activation/'.urlencode(base64_encode($userID)));
        $emaildata['name'] = $this->lang->line('dear').$firstName.' '.$lastName;
        $emaildata['message'] = $this->lang->line('verify_account_message');
        $replaceto = array("activationlink__");
        $replacewith = array($activationlink);
        $emaildata['message'] = str_replace($replaceto, $replacewith, $emaildata['message']);
        $content = $this->load->view('success_email',$emaildata, TRUE);
        send_email($email,$this->lang->line('registration_email'),$content);   
    }    
}
