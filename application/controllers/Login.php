<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_controller
{
    public function __construct(){
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
    }
    public function verifycredentials(){  
        $data = array();

        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));
        $token = $this->input->post('token');
        
        if($email=='' || $password=='' || $token=='')   
            return response_parameter_missing_message();
              
        $message= $this->verify_details($email,$password);
       
        if($message!='')
            return response_json_output(false,$message,$data);
       
        $userID=$this->insert_token($email,$token);
        
        $userData=$this->get_user_data($userID);
        
            return response_success_message($userData);    
    }
    private function insert_token($email,$token){
        $userInfo=$this->DataAccessLayer->getAll('UserMaster',array('Email'=>$email))[0];
            $this->DataAccessLayer->update('UserMaster',array('Email'=>$email),array('Token'=>$token));
            $this->DataAccessLayer->logData('UserMaster',$userInfo->ID,$userInfo->ID);  
        return $userInfo->ID;
    }
    private function verify_details($email,$password){
        $result=$this->DataAccessLayer->getAll('UserMaster',array('Email'=>$email));
        if(($result==NULL) || (count($result) == 0))
            return $this->lang->line('email_not_register');
        $result=$this->DataAccessLayer->getAll('UserMaster',array('Email'=>$email,'Password'=>$password));
        if(($result==NULL) || (count($result) == 0))
            return $this->lang->line('password_incorrect');
        return '';            
    }
    protected function get_user_data($userID){       
        $this->db->select('u.ID as UserID,u.FirstName,u.LastName,u.Email');
        $this->db->from('UserMaster as u');
        $this->db->where(array('u.ID'=>$userID));
        $userData=$this->db->get()->row_array();
        
        return $userData;
        
    }  

}    