<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Forgotpassword extends CI_controller
{
    public function __construct(){
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
    }
    public function verifycredentials(){  
        try{
            $data = array();
                        
            $email = trim($this->input->post('email'));

             if($email=='')   
                return response_parameter_missing_message();
         
            $userInfo=$this->DataAccessLayer->getAll('UserMaster',array('Email'=>$email));
          
            if(isset($userInfo) && empty($userInfo)){
                response_json_output(false,$this->lang->line('email_not_register'),$data);
                return;
            }
           
            $userID=$userInfo[0]->ID;
            $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('Token'=>generate_randomNumber(9)));
            $this->DataAccessLayer->logData('UserMaster',$userID,$userID); 

            $this->send_Mail($userID,$email); 

            return response_success_message($data);    
        }
        catch(Exception $exception){
            
           response_exception_message($exception);
        } 
    }
    public function resetpassword($id=''){
        $data = array();
        if(empty($id)) return; 

        $userID= $data['userID'] =  base64_decode(urldecode($id));

        $userInfo=$this->DataAccessLayer->getAll('UserMaster',array('ID'=>$userID))[0];
          
            if(isset($userInfo->TokenID) && empty($userInfo->TokenID)){
               $data['message'] = $this->lang->line('password_link_expire');
               $this->load->view('Forgotpassword_email',$data); 
               return;
            }
        $this->load->view('Resetpassword',$data);
    }
    private function send_Mail($userID,$email)
    {
        $activationlink = base_url('forgotpassword/resetpassword/'.urlencode(base64_encode($userID)));
        $emaildata['message'] = $this->lang->line('forgotpassword_email_link');
        $replaceto = array("activationlink__");
        $replacewith = array($activationlink);
        $emaildata['message'] = str_replace($replaceto, $replacewith, $emaildata['message']);
        $content = $this->load->view('Forgotpassword_email',$emaildata, TRUE);
        send_email($email,$this->lang->line('forgot_password'),$content);  
    }
    
    public function password(){
        try{
            $data = array();
            if(!empty($this->input->post())){
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('UserNewPassword', 'New Password','required|min_length[6]');
                $this->form_validation->set_rules('UserConfirmPassword','Confirm Password','required|min_length[6]|matches[UserNewPassword]'); 
                if ($this->form_validation->run() == FALSE) {
                    $data['UserInfo'] = array(
                        'UserNewPassword'=>$this->input->post('UserNewPassword'),
                        'UserConfirmPassword'=>$this->input->post('UserConfirmPassword'),
                    );
                    $data['userID'] =  $this->input->post('userid');
                    $this->load->view('Resetpassword',$data);
                }
                else {
                    $userID=$this->input->post('userid');
                    $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('Password'=>md5($this->input->post('UserNewPassword')),'Token'=>""));
                    $this->DataAccessLayer->logData('UserMaster',$userID,$userID); 
                    $data['message'] = $this->lang->line('password_changed_success');
                    $this->load->view('Forgotpassword_email',$data); 
                }   
            }
        }
        catch(Exception $exception){            
           response_exception_message($exception);
        } 
    }
}