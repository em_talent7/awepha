<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Logout extends CI_controller
{
    public function __construct(){
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
    }
    public function index(){
         try{ 
            $data = array();    

            $userID = $this->input->post('userid'); 
           
            if($userID=='')   
                return response_parameter_missing_message();
            
            $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('Token'=>''));
            $this->DataAccessLayer->logData('UserAddressLink',$userID,$userID);
            return response_success_message($data);   
        }
        catch(Exception $exception){
            response_exception_message($exception);
        }
    }
}    