<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pages extends CI_controller
{
	 public function __construct(){
        parent::__construct(); 
       // $this->lang->load('stringResources_lang');
      //  check_valid_user_token($this->input->post('currentuserid'),$this->input->post('tokenid'));       
    }	
	public function terms(){
	    try{
            $where = array('Status'=>'Y','PageSlug'=>'term-and-service');
            $pages['termdata'] = $this->DataAccessLayer->getAll('PageMaster',$where)[0];
             if($pages!=null && count($pages)!=0)
            {
                $this->load->view('Page',$pages);
            }
        }
        catch(Exception $exception){
           response_exception_message($exception);
        }
    }
    public function privacy(){
	    try{
            $where = array('Status'=>'Y','PageSlug'=>'privacy-policy');
            $pages['termdata'] = $this->DataAccessLayer->getAll('PageMaster',$where)[0];
             if($pages!=null && count($pages)!=0)
            {
                $this->load->view('Page',$pages);
            }
        }
        catch(Exception $exception){
           response_exception_message($exception);
        }
    }
    public function about(){
	    try{
            $where = array('Status'=>'Y','PageSlug'=>'about-us');
            $pages['termdata'] = $this->DataAccessLayer->getAll('PageMaster',$where)[0];
             if($pages!=null && count($pages)!=0)
            {
                $this->load->view('Page',$pages);
            }
        }
        catch(Exception $exception){
           response_exception_message($exception);
        }
	}
}
?>