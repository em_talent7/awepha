<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(dirname(__FILE__) . "/Login.php");
class Profile extends Login
{
    public function __construct() {
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
    }
    public function saveuser(){
        try{
            $data = array();
            $userID=$this->input->post('userid');
            $firstName=ucfirst($this->input->post('firstname'));
            $lastName=ucfirst($this->input->post('lastname'));
            
            

            if($userID==''  || $firstName=='' || $lastName=='')  
                return response_parameter_missing_message();
                
             $userInfo=$this->get_user_data($userID);    
            

                   
            $this->db->trans_begin();     
            $this->update_user($firstName,$lastName,$userID);
            
            if ($this->db->trans_status() === FALSE)  $this->db->trans_rollback();               
            else $this->db->trans_commit();

            $userInfo=$this->get_user_data($userID); 

            return response_success_message($userInfo);       
        }
       catch(Exception $exception){
           response_exception_message($exception);
       }
    }
    public function saveaddress(){
        try{
            $data = array();
            $userID=$this->input->post('userid');
            $addressTitle=$this->input->post('addresstitle');
            $address=$this->input->post('address');
            $city=$this->input->post('city');
            $state=$this->input->post('state');
            $country=$this->input->post('country');
            $latitude=$this->input->post('latitude');
            $longitude=$this->input->post('longitude');
            $isCurrent=$this->input->post('iscurrentaddress');
            $phoneNumber=$this->input->post('phonenumber');
            $zipcode=$this->input->post('zipcode');
            
            if($userID=='' || $address=='')  
                return response_parameter_missing_message();
  
            $this->db->trans_begin(); 
            $this->manage_address($userID,$phoneNumber,$zipcode,$address,$country,$state,$city,$latitude,$longitude,$addressTitle,$isCurrent);
            if ($this->db->trans_status() === FALSE)  $this->db->trans_rollback();               
            else $this->db->trans_commit();

           $userInfo=$this->get_user_data($userID); 
           return response_success_message($userInfo['UserAddresses']);         
        }
        catch(Exception $exception){
           response_exception_message($exception);
       }
    }
     public function getreferalcode(){
        try{
            $data = array();
            $userID=$this->input->post('userid');

            if($userID=='')  
                return response_parameter_missing_message();

            $data=$this->DataAccessLayer->getAll('UserMaster',array('ID'=>$userID));

            if($data!=null && count($data)!=0)
                 return response_success_message($data[0]->ReferalCode); 

            $userInfo=$this->get_user_data($userID); 
            return response_success_message($userInfo['UserAddresses']);       
        }
        catch(Exception $exception){
           response_exception_message($exception);
       }
    }
    public function getuseraddress(){
        try{
            $data = array();
            $userID=$this->input->post('userid');

            if($userID=='' )  
                return response_parameter_missing_message();
  
            $userInfo=$this->get_user_data($userID); 
            if($userInfo['UserAddresses']!=null && count($userInfo['UserAddresses'])!=0)
                 return response_success_message($userInfo['UserAddresses']);    
            return response_fail_message($this->lang->line('no_result_found')); 
        }
        catch(Exception $exception){
           response_exception_message($exception);
       }
    }
    public function makecurrentaddress(){
        try{
            $data = array();
            $userID=$this->input->post('userid');
            $addressid=$this->input->post('addressid');
            $isCurrent=$this->input->post('iscurrentaddress');

            if($userID=='' || $addressid=='' || $isCurrent=='')  
                return response_parameter_missing_message();
  
            $this->db->trans_begin(); 
            $this->update_user_address_status($userID,$addressid,$isCurrent);
            if ($this->db->trans_status() === FALSE)  $this->db->trans_rollback();               
            else $this->db->trans_commit();

            $userInfo=$this->get_user_data($userID); 
            return response_success_message($userInfo['UserAddresses']);       
        }
        catch(Exception $exception){
           response_exception_message($exception);
       }
    }
    private function update_user($firstName,$lastName,$userID){
        $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('FirstName'=>$firstName,'lastName'=>$lastName,'IsCompleted'=>'Y'));
        $this->DataAccessLayer->logData('UserMaster',$userID,$userID);  
    }
    private function manage_user($userID,$imageUrl="",$DOB=""){
       $rowCount=count($this->DataAccessLayer->getAll('UserMetaLink',array('UserID'=>$userID)));
       if($rowCount==0){
           $id= $this->DataAccessLayer->insert('UserMetaLink',array('UploadedProofPath'=>$imageUrl ,'DOB'=>$DOB,'UserID'=>$userID));
       }else{
            $this->DataAccessLayer->update('UserMetaLink',array('UserID'=>$userID),array('UploadedProofPath'=>$imageUrl,'DOB'=>$DOB));
            $id=$this->DataAccessLayer->getAll('UserMetaLink',array('UserID'=>$userID))[0]->ID;
       }
       $this->DataAccessLayer->logData('UserMetaLink',$userID,$id);
    }
    private function manage_address($userID,$phoneNumber,$zipcode,$address="",$country="",$state,$city="",$latitude="",$Longitude="",$addressTitle,$isCurrent,$addressid=""){
        if(empty($addressid)){
           $id= $this->DataAccessLayer->insert('UserAddressLink',array('UserID'=>$userID,'PhoneNumber'=>$phoneNumber,'Address'=>$address,'Country'=>$country,'State'=>$state,'City'=>$city,'Latitude'=>$latitude,'Longitude'=>$Longitude,'IsCurretAddress'=>$isCurrent,'AddressTitle'=>$addressTitle,'Zipcode'=>$zipcode));
        }else{
            $this->DataAccessLayer->update('UserAddressLink',array('UserID'=>$userID),array('IsCurretAddress'=>'N'));
            $this->DataAccessLayer->update('UserAddressLink',array('UserID'=>$userID,'ID'=>$addressid),array('PhoneNumber'=>$phoneNumber,'Address'=>$address,'Country'=>$country,'State'=>$state,'City'=>$city,'Latitude'=>$latitude,'Longitude'=>$Longitude,'IsCurretAddress'=>$isCurrent,'AddressTitle'=>$addressTitle,'Zipcode'=>$zipcode));
            $id=$this->DataAccessLayer->getAll('UserAddressLink',array('UserID'=>$userID,'ID'=>$addressid))[0]->ID;
        }
       $this->DataAccessLayer->logData('UserAddressLink',$userID,$id);
       return $id;
    } 
    private function update_user_address_status($userID,$addressid,$isCurrent){
        $this->DataAccessLayer->update('UserAddressLink',array('UserID'=>$userID),array('IsCurretAddress'=>'N'));
        $this->DataAccessLayer->update('UserAddressLink',array('UserID'=>$userID,'ID'=>$addressid),array('IsCurretAddress'=>$isCurrent));
        $this->DataAccessLayer->logData('UserAddressLink',$userID,$userID);  
    }
    private function checkAddress($userID,$addressid=''){
        $result=$this->DataAccessLayer->getAll('UserAddressLink',array('UserID'=>$userID));
        if(!empty($result) && empty($addressid)) 
                return $this->lang->line('provide_address_id');
        if(!empty($addressid)){
            $result=$this->DataAccessLayer->getAll('UserAddressLink',array('UserID'=>$userID,'ID'=>$addressid));
             if(empty($result)) 
                return $this->lang->line('addressid_not_exist');
        }
        return ''; 
    } 
}