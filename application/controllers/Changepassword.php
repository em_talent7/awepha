<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Changepassword extends CI_controller
{
    public function __construct(){
        parent::__construct(); 
        $this->lang->load('string_resources_lang');
    }
    public function verifycredentials(){  
        try{
            $data = array();
                        
            $userid = trim($this->input->post('userid'));
            $current_password = trim($this->input->post('current_password'));
            $new_password = trim($this->input->post('new_password'));
            $confirm_password = trim($this->input->post('confirm_password'));

             if($userid=='' || $current_password =='' || $new_password =='' || $confirm_password=='')   
                return response_parameter_missing_message();
         
            $userExist=$this->DataAccessLayer->getAll('UserMaster',array('ID'=>$userid));
          
          if(isset($userExist) && empty($userExist)){
                response_json_output(false,$this->lang->line('userid_not_register'),$data);
                return;
            }
           
          
          $userInfo=$this->DataAccessLayer->getAll('UserMaster',array('ID'=>$userid, 'password'=>md5($current_password)));
          
            if(isset($userInfo) && empty($userInfo)){
                response_json_output(false,$this->lang->line('current_password_incorrect'),$data);
                return;
            }
            
           
          
            if($new_password != $confirm_password){
                response_json_output(false,$this->lang->line('newpassword_confirmpassword_incorrect'),$data);
                return;
            }
            
            $userID=$userInfo[0]->ID;
            $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('password'=>md5($new_password)));
            $this->DataAccessLayer->logData('UserMaster',$userID,$userID); 

            return response_success_message($data);    
        }
        catch(Exception $exception){
            
           response_exception_message($exception);
        } 
    }
    public function resetpassword($id=''){
        $data = array();
        if(empty($id)) return; 

        $userID= $data['userID'] =  base64_decode(urldecode($id));

        $userInfo=$this->DataAccessLayer->getAll('UserMaster',array('ID'=>$userID))[0];
          
            if(isset($userInfo->TokenID) && empty($userInfo->TokenID)){
               $data['message'] = $this->lang->line('password_link_expire');
               $this->load->view('Forgotpassword_email',$data); 
               return;
            }
        $this->load->view('Resetpassword',$data);
    }
    
    
    public function password(){
        try{
            $data = array();
            if(!empty($this->input->post())){
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('UserNewPassword', 'New Password','required|min_length[6]');
                $this->form_validation->set_rules('UserConfirmPassword','Confirm Password','required|min_length[6]|matches[UserNewPassword]'); 
                if ($this->form_validation->run() == FALSE) {
                    $data['UserInfo'] = array(
                        'UserNewPassword'=>$this->input->post('UserNewPassword'),
                        'UserConfirmPassword'=>$this->input->post('UserConfirmPassword'),
                    );
                    $data['userID'] =  $this->input->post('userid');
                    $this->load->view('Resetpassword',$data);
                }
                else {
                    $userID=$this->input->post('userid');
                    $this->DataAccessLayer->update('UserMaster',array('ID'=>$userID),array('Password'=>md5($this->input->post('UserNewPassword')),'Token'=>""));
                    $this->DataAccessLayer->logData('UserMaster',$userID,$userID); 
                    $data['message'] = $this->lang->line('password_changed_success');
                    $this->load->view('Forgotpassword_email',$data); 
                }   
            }
        }
        catch(Exception $exception){            
           response_exception_message($exception);
        } 
    }
}