<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_controller
{
	public function __construct()
	{
		parent::__construct();
	}	
	public function page404(){
		$this->load->view('404');
	}
	public function index(){
		$this->load->view('Home');
	}
}
?>
