<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function send_email($To,$Subject,$Content) {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "webmail.elintminds.work";
        $config['smtp_port'] = "2525";
        $config['smtp_user'] = "awepha@elintminds.work"; 
        $config['smtp_pass'] = "awepha@123";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $ci->email->initialize($config);
        $ci->email->from('awepha@elintminds.work', 'Awepha');
        // $list = array('em_talent6@elintminds.com');
        $ci->email->to($To);
        $ci->email->subject($Subject);
        $ci->email->message($Content);
        $result = $ci->email->send();
        log_message('debug', $ci->email->print_debugger());
		return $result;
    }
	function generate_randomNumber($length = 8) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $key = '';
        for ($i = 0; $i < $length; $i++) {
            $key .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $key;
    }
    function response_json_output($status,$message,$response){
    	echo json_encode(array('status'=>$status,'message'=>$message,'response'=>$response));
    }    
    function response_parameter_missing_message(){
        $message="Parameters are missing";
        response_failure_message($message);
    }    
    function response_fail_message($message){
        response_failure_message($message);
    }
    function response_exception_message($exception){
        $message="An exception occured while performing the action. ".$exception.getMessage();
        response_failure_message($message);
    }
    function response_failure_message($message){
    	echo json_encode(array('status'=>false,'message'=>$message,'response'=>(object)NULL));
    }
    function response_success_message($response){
    	echo json_encode(array('status'=>true,'message'=>"Success",'response'=>$response));
    }
    function send_push_notification_to_fcm_sever($token,$userid,$receiverid,$title,$content,$notifyid='',$type) {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => array($token),
            'priority' => 1,
            'notification' => array('title' => $title,'body' =>$content ,'sound'=>'Default'),
            'data' => array('notificationid'=>$notifyid,'notificationtype'=>$type,'userid'=>$userid,'receiverid'=>$receiverid),
        );
        $headers = array(
            'Authorization:key='.FIREBASE_API_SERVER_KEY,
            'Content-Type:application/json'
        );        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }    
    function printt($data){
    	echo'<pre>';
    	print_r($data);
    	echo'</pre>';
    }
    function images_upload($data,$fieldname,$path){
        foreach($data[$fieldname]['name'] as $key=>$val){
        $image_name = $data[$fieldname]['name'][$key];
        $tmp_name   = $data[$fieldname]['tmp_name'][$key];        
        $file = basename($data[$fieldname]['name'][$key]);            
            $filePath=$path.'/'.$file;
            try{
                if (!file_exists($path)) 
                    mkdir($path, 0777, true);

                if (file_exists($filePath)) 
                    unlink($filePath);

                if(move_uploaded_file($_FILES[$fieldname]['tmp_name'][$key],$filePath))
                    $images_arr[] = $filePath;

            }catch(Exception $e){
                echo $this->lang->line('file_can_not_upload');
            }        
        }
        return $images_arr;
    }
    function add_notification($typeID,$senderID,$type,$title,$content,$message,$receiverID,$subject,$targetID){
        $ci = &get_instance();
        $ci->lang->load('string_resources_lang');
        $emaildata=array();       
		$notificationID=$ci->DataAccessLayer->insert('NotificationLink',array('TargetID'=>$targetID,'TypeID'=>$typeID,'SenderID'=>$senderID,'ReceiverID'=>$receiverID,'Type'=>$type,'Title'=>$title,'Content'=>$content));
        $ci->DataAccessLayer->logData('NotificationLink',$senderID,$notificationID); 
        if(isset($receiverID) && !empty($receiverID)){
            $userData=get_user_by_ID($receiverID);     
            $email = $userData['Email'];
            $emaildata['name'] = $ci->lang->line('dear').$userData['FirstName'].' '.$userData['LastName'];
            $emaildata['message'] = $message;
            $emaicontent = $ci->load->view('success_email',$emaildata, TRUE);
            $token=$userData['TokenID'];
            if($senderID !=$receiverID){
                sendEmail($email,$subject,$emaicontent);
                if(isset($token) && !empty($token))
                    sendPushNotificationToFCMSever($token,$senderID,$receiverID,$title,$content,$notificationID,$typeID);   
            }
        }
    }
    function get_user_by_ID($userID){ 
        $ci = &get_instance();
        $ci->db->select('u.FirstName,u.LastName,u.Email,u.TokenID');
        $ci->db->from('UserMaster as u');
        $ci->db->join('UserMetaLink as uml','uml.UserID=u.ID','left outer');  
        $ci->db->where(array('u.ID'=>$userID));
        $query=$ci->db->get();
        return $query->row_array();
    } 

	
	